package client

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/usvc/modules/go/calendar/constants"
)

type Client struct {
	URL string
}

type DiscoverResponse struct {
	AllowedMethods []string
	DAVs           []string
}

// Discover implements RFC4791 Section 5.1.1 on discovery of
// calendar access support.
//
// See https://tools.ietf.org/html/rfc4791#section-5.1.1
func (c Client) Discover() (*DiscoverResponse, error) {
	requestURL, err := url.Parse(c.URL)
	if err != nil {
		return nil, err
	}
	response, err := (&http.Client{
		Timeout: 30 * time.Second,
	}).Do(&http.Request{
		Host:   requestURL.Hostname(),
		URL:    requestURL,
		Method: constants.MethodOptions,
	})
	if err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("server responded with status code %v", response.StatusCode)
	}
	allows := strings.Split(response.Header.Get("Allow"), ",")
	for i := 0; i < len(allows); i++ {
		allows[i] = strings.Trim(allows[i], " ")
	}
	davs := strings.Split(response.Header.Get("DAV"), ",")
	for i := 0; i < len(davs); i++ {
		davs[i] = strings.Trim(davs[i], " ")
	}
	return &DiscoverResponse{
		AllowedMethods: allows,
		DAVs:           davs,
	}, nil
}
