package client

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/calendar/constants"
)

type ClientTests struct {
	suite.Suite
}

func TestClient(t *testing.T) {
	suite.Run(t, &ClientTests{})
}

func (s *ClientTests) TestDiscover() {
	var hostname string
	var request *http.Request
	defer func() {
		s.Equal(constants.MethodOptions, request.Method)
		s.Equal(hostname, request.Host)
	}()
	server := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			request = r
			w.WriteHeader(http.StatusOK)
		}),
	)
	testServerURL, err := url.Parse(server.URL)
	s.Nil(err)
	hostname = testServerURL.Hostname()
	c := Client{
		URL: server.URL,
	}
	_, err = c.Discover()
	s.Nil(err)
}
