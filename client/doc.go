/*
The `client` package implements accessing a CalDAV server according
to RFC4791. To know more, see Section 5 on Calendar Access Feature at
https://tools.ietf.org/html/rfc4791#section-5
*/
package client
