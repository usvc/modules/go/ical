package event

import (
	"strings"
	"time"

	"gitlab.com/usvc/modules/go/calendar/properties"
)

type Event struct {
	Categories   *properties.Categories
	Contact      *properties.Contact
	Created      *properties.Created
	Description  *properties.Description
	DTEnd        *properties.DTEnd
	DTStamp      *properties.DTStamp
	DTStart      *properties.DTStart
	LastModified *properties.LastModified
	Location     *properties.Location
	Organizer    *properties.Organizer
	Status       *properties.Status
	Summary      *properties.Summary
	UID          *properties.UID
	URL          *properties.URL

	IsAllDay bool
}

// String returns the value as a valid iCalendar Event item
func (event Event) String() string {
	var p strings.Builder

	if event.IsAllDay {
		event.DTEnd.IsDate = true
		event.DTEnd.Value = event.DTEnd.Value.Add(24 * time.Hour)
		event.DTStart.IsDate = true
	}

	p.WriteString("BEGIN:VEVENT\n")
	if event.Categories != nil && event.Categories.IsValid() {
		p.WriteString(event.Categories.String())
	}
	if event.Contact != nil && event.Contact.IsValid() {
		p.WriteString(event.Contact.String())
	}
	if event.Description != nil && event.Description.IsValid() {
		p.WriteString(event.Description.String())
	}
	if event.DTEnd != nil && event.DTEnd.IsValid() {
		p.WriteString(event.DTEnd.String())
	}
	if event.DTStamp != nil && event.DTStamp.IsValid() {
		p.WriteString(event.DTStamp.String())
	}
	if event.DTStart != nil && event.DTStart.IsValid() {
		p.WriteString(event.DTStart.String())
	}
	if event.LastModified != nil && event.LastModified.IsValid() {
		p.WriteString(event.LastModified.String())
	}
	if event.Location != nil && event.Location.IsValid() {
		p.WriteString(event.Location.String())
	}
	if event.Organizer != nil && event.Organizer.IsValid() {
		p.WriteString(event.Organizer.String())
	}
	if event.Status != nil && event.Status.IsValid() {
		p.WriteString(event.Status.String())
	}
	if event.Summary != nil && event.Summary.IsValid() {
		p.WriteString(event.Summary.String())
	}
	if event.UID != nil && event.UID.IsValid() {
		p.WriteString(event.UID.String())
	}
	if event.URL != nil && event.URL.IsValid() {
		p.WriteString(event.URL.String())
	}

	p.WriteString("END:VEVENT\n")
	return p.String()
}
