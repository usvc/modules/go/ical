package event

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/usvc/modules/go/calendar/properties"
)

// New returns a new instance of the Event struct
func New() *Event {
	now := time.Now().UTC()

	event := &Event{}

	uid := properties.UID(uuid.New().String())
	event.UID = &uid

	dtStamp := properties.DTStamp(now)
	event.DTStamp = &dtStamp

	created := properties.Created(now)
	event.Created = &created

	lastModified := properties.LastModified(now)
	event.LastModified = &lastModified

	return event
}

// FromDatetime adds a starting date/time stamp in UTC to the Event instance
func (event *Event) FromDatetime(year, month, date, hour, minute, second int) *Event {
	utc, _ := time.LoadLocation("UTC")
	start := properties.DTStart{Value: time.Date(year, time.Month(month), date, hour, minute, second, 0, utc)}
	event.DTStart = &start
	return event
}

// FromDatetime adds a starting date/time stamp in the local machine's time to the Event instance
func (event *Event) FromDatetimeLocal(year, month, date, hour, minute, second int) *Event {
	location := time.Now().Location()
	start := properties.DTStart{Value: time.Date(year, time.Month(month), date, hour, minute, second, 0, location)}
	event.DTStart = &start
	return event
}

// ToDatetime adds an ending date/time stamp in UTC to the Event instance
func (event *Event) ToDatetime(year, month, date, hour, minute, second int) *Event {
	utc, _ := time.LoadLocation("UTC")
	end := properties.DTEnd{Value: time.Date(year, time.Month(month), date, hour, minute, second, 0, utc)}
	event.DTEnd = &end
	return event
}

// ToDatetimeLocal adds an ending date/time stamp in the local machine's time to the Event instance
func (event *Event) ToDatetimeLocal(year, month, date, hour, minute, second int) *Event {
	location := time.Now().Location()
	end := properties.DTEnd{Value: time.Date(year, time.Month(month), date, hour, minute, second, 0, location)}
	event.DTEnd = &end
	return event
}

// withTitle adds a title to the Event instance
func (event *Event) WithTitle(title string) *Event {
	summary := properties.Summary{Text: title}
	event.Summary = &summary
	return event
}

// WithDescription adds a description to the Event instance
func (event *Event) WithDescription(description string) *Event {
	event.Description = &properties.Description{Text: description}
	return event
}
