package event

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/calendar/constants"
	"gitlab.com/usvc/modules/go/calendar/properties"
)

type EventTests struct {
	suite.Suite
}

func TestEvents(t *testing.T) {
	suite.Run(t, &EventTests{})
}

func (t *EventTests) TestStringWithoutParams() {
	event := Event{}
	eventString := event.String()
	t.Equal("BEGIN:VEVENT\nEND:VEVENT\n", eventString)
}

func (t *EventTests) TestStringWithCategories() {
	expectedCategories := properties.Categories([]string{"CAT1", "CAT2", "CAT3"})
	event := Event{
		Categories: &expectedCategories,
	}
	eventString := event.String()
	t.Contains(eventString, "CATEGORIES:CAT1,CAT2,CAT3")
}

func (t *EventTests) TestStringWithContact() {
	expectedContact := properties.Contact{Text: "contact person <email@domain.com>"}
	event := Event{
		Contact: &expectedContact,
	}
	eventString := event.String()
	t.Contains(eventString, "CONTACT:contact person <email@domain.com>")
}

func (t *EventTests) TestStringWithDescription() {
	expectedDescription := properties.Description{Text: "test description"}
	event := Event{
		Description: &expectedDescription,
	}
	eventString := event.String()
	t.Contains(eventString, "DESCRIPTION:\"test description\"")
}

func (t *EventTests) TestStringWithDateTimeEnd() {
	location, _ := time.LoadLocation("UTC")
	expectedDTEnd := properties.DTEnd{Value: time.Date(2019, 12, 31, 23, 59, 59, 0, location)}
	event := Event{
		DTEnd: &expectedDTEnd,
	}
	eventString := event.String()
	t.Contains(eventString, "DTEND:20191231T235959Z")
}

func (t *EventTests) TestStringWithDateTimeStamp() {
	location, _ := time.LoadLocation("UTC")
	expectedDTStamp := properties.DTStamp(time.Date(2019, 12, 31, 23, 59, 59, 0, location))
	event := Event{
		DTStamp: &expectedDTStamp,
	}
	eventString := event.String()
	t.Contains(eventString, "DTSTAMP:20191231T235959Z")
}

func (t *EventTests) TestStringWithDateTimeStart() {
	location, _ := time.LoadLocation("UTC")
	expectedDTStart := properties.DTStart{Value: time.Date(2019, 12, 31, 23, 59, 59, 0, location)}
	event := Event{
		DTStart: &expectedDTStart,
	}
	eventString := event.String()
	t.Contains(eventString, "DTSTART:20191231T235959Z")
}

func (t *EventTests) TestStringWithLastModified() {
	location, _ := time.LoadLocation("UTC")
	expectedLastModified := properties.LastModified(time.Date(2019, 12, 31, 23, 59, 59, 0, location))
	event := Event{
		LastModified: &expectedLastModified,
	}
	eventString := event.String()
	t.Contains(eventString, "LAST-MODIFIED:20191231T235959Z")
}

func (t *EventTests) TestStringWithLocation() {
	expectedLocation := properties.Location{Text: "some location"}
	event := Event{
		Location: &expectedLocation,
	}
	eventString := event.String()
	t.Contains(eventString, "LOCATION:\"some location\"")
}

func (t *EventTests) TestStringWithOrganizer() {
	expectedOrganizer := properties.Organizer{Mailto: "someone@somewhere.com"}
	event := Event{
		Organizer: &expectedOrganizer,
	}
	eventString := event.String()
	t.Contains(eventString, "ORGANIZER:MAILTO:\"someone@somewhere.com\"")
}

func (t *EventTests) TestStringWithStatus() {
	expectedStatus := properties.Status(constants.StatusEventTentative)
	event := Event{
		Status: &expectedStatus,
	}
	eventString := event.String()
	t.Contains(eventString, fmt.Sprintf("STATUS:%s", constants.StatusEventTentative))
}

func (t *EventTests) TestStringWithSummary() {
	expectedSummary := properties.Summary{Text: "__test_summary"}
	event := Event{
		Summary: &expectedSummary,
	}
	eventString := event.String()
	t.Contains(eventString, "SUMMARY:__test_summary")
}

func (t *EventTests) TestStringWithUID() {
	expectedUID := properties.UID("uuid")
	event := Event{
		UID: &expectedUID,
	}
	eventString := event.String()
	t.Contains(eventString, "UID:uuid")
}

func (t *EventTests) TestStringWithURL() {
	expectedURL := properties.URL("https://google.com")
	event := Event{
		URL: &expectedURL,
	}
	eventString := event.String()
	t.Contains(eventString, "URL:https://google.com")
}
