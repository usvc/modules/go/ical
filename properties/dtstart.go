package properties

import (
	"strings"
	"time"
)

// DTStart implements 4.8.2.4 Date/Time Start
//
// dtstart    = "DTSTART" dtstparam ":" dtstval CRLF
// dtstparam  = *((";" "VALUE" "=" ("DATE-TIME" / "DATE")) / (";" tzidparam) / *(";" xparam))
// dtstval    = date-time / date
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.2.4
type DTStart struct {
	// Value specifies the starting date/date-time
	Value time.Time
	// TZID specifies the timezone ID
	TZID string
	// IsDate specifies whether the value should be a DATE or DATE-TIME value
	IsDate bool
}

// IsValid implements the Property interface
func (property DTStart) IsValid() bool {
	return property.Value != time.Time{}
}

// String returns the value as a valid iCalendar DTStart property
func (property DTStart) String() string {
	var p strings.Builder
	p.WriteString("DTSTART")
	if len(property.TZID) > 0 {
		p.WriteString(";TZID=")
		p.WriteString(property.TZID)
	}
	p.WriteByte(':')
	if property.IsDate {
		p.WriteString(FormatDate(property.Value))
	} else if len(property.TZID) > 0 {
		p.WriteString(FormatDateTimeLocal(property.Value))
	} else {
		p.WriteString(FormatDateTime(property.Value))
	}
	p.WriteByte('\n')
	return p.String()
}
