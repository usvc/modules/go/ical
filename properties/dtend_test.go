package properties

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type DTEndTests struct {
	suite.Suite
}

func TestDTEnd(t *testing.T) {
	suite.Run(t, &DTEndTests{})
}
func (t *DTEndTests) TestMatchProperty() {
	var dtEnd Property
	dtEnd = DTEnd{}
	t.Equal("DTEND:00010101T000000Z\n", dtEnd.String())
}

func (t *DTEndTests) TestIsValid() {
	t.False(DTEnd{}.IsValid())
	t.False(DTEnd{Value: time.Time{}}.IsValid())
	utcLocation, _ := time.LoadLocation("UTC")
	t.True(DTEnd{Value: time.Date(1, 1, 1, 1, 1, 1, 1, utcLocation)}.IsValid())
}

func (t *DTEndTests) TestString() {
	utcLocation, _ := time.LoadLocation("UTC")
	testTime := time.Date(2019, 12, 13, 14, 59, 1, 2, utcLocation)
	dtEnd := DTEnd{
		Value: testTime,
	}
	t.Equal("DTEND:20191213T145901Z\n", dtEnd.String())
}

func (t *DTEndTests) TestStringWithParams() {
	singapore, _ := time.LoadLocation("Asia/Singapore")
	testTime := time.Date(2019, 12, 13, 14, 59, 1, 2, singapore)
	dtEnd := DTEnd{
		Value: testTime,
		TZID:  "Asia/Singapore",
	}
	t.Equal("DTEND;TZID=Asia/Singapore:20191213T145901\n", dtEnd.String())
}
