package properties

import (
	"strings"
)

// Status implements 4.8.1.11 Status
//
// status     = "STATUS" statparam] ":" statvalue CRLF
// statparam  = *(";" xparam)
// statvalue  =
//   "TENTATIVE"          ;Indicates event is tentative.
// / "CONFIRMED"          ;Indicates event is definite.
// / "CANCELLED"          ;Indicates event was cancelled.
// ;Status values for a "VEVENT"
// statvalue  =/
//   "NEEDS-ACTION"       ;Indicates to-do needs action.
// / "COMPLETED"          ;Indicates to-do completed.
// / "IN-PROCESS"         ;Indicates to-do in process of
// / "CANCELLED"          ;Indicates to-do was cancelled.
// ;Status values for "VTODO".
// statvalue  =/
//   "DRAFT"              ;Indicates journal is draft.
// / "FINAL"              ;Indicates journal is final.
// / "CANCELLED"          ;Indicates journal is removed.
// ;Status values for "VJOURNAL".
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.1.11
type Status string

// IsValid implements the Property interface
func (property Status) IsValid() bool {
	return len(property) > 0
}

// String returns the value as a valid iCalendar Status property
func (property Status) String() string {
	var p strings.Builder
	p.WriteString("STATUS")
	p.WriteByte(':')
	p.WriteString(string(property))
	p.WriteByte('\n')
	return p.String()
}
