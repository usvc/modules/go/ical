package properties

import (
	"strings"
)

// Organizer implements 4.8.4.3 Organizer
//
// organizer  = "ORGANIZER" orgparam ":" cal-address CRLF
// orgparam   = *((";" cnparam) / (";" dirparam) / (";" sentbyparam) / (";" languageparam) / *(";" xparam))
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.4.3
type Organizer struct {
	CN       string
	Dir      string
	SentBy   string
	Language string
	Mailto   string
}

// IsValid implements the Property interface
func (property Organizer) IsValid() bool {
	return len(property.Mailto) > 0
}

// String returns the value as a valid iCalendar Organizer property
func (property Organizer) String() string {
	var p strings.Builder
	p.WriteString("ORGANIZER")
	if len(property.CN) > 0 {
		p.WriteString(";CN=")
		p.WriteString(property.CN)
	}
	if len(property.Dir) > 0 {
		p.WriteString(";DIR=")
		p.WriteByte('"')
		p.WriteString(property.Dir)
		p.WriteByte('"')
	}
	if len(property.SentBy) > 0 {
		p.WriteString(";SENT-BY=")
		p.WriteByte('"')
		p.WriteString("MAILTO:")
		p.WriteString(property.SentBy)
		p.WriteByte('"')
	}
	if len(property.Language) > 0 {
		p.WriteString(";LANGUAGE=")
		p.WriteString(property.Language)
	}
	if len(property.Mailto) > 0 {
		p.WriteString(":MAILTO:\"")
		p.WriteString(property.Mailto)
		p.WriteByte('"')
	}
	p.WriteByte('\n')
	return p.String()
}
