package properties

import "strings"

// Description implements 4.8.1.5 Description
//
// description = "DESCRIPTION" descparam ":" text CRLF
// descparam  = *((";" altrepparam) / (";" languageparam) / *(";" xparam))
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.1.5
type Description struct {
	Altrep   string
	Language string
	Text     string
}

// IsValid implements the Property interface
func (property Description) IsValid() bool {
	return len(property.Altrep) > 0 || len(property.Text) > 0
}

// String returns the value as a valid iCalendar Organizer property
func (property Description) String() string {
	var p strings.Builder
	p.WriteString("DESCRIPTION")

	if len(property.Altrep) > 0 {
		p.WriteString(`;ALTREP="`)
		p.WriteString(property.Altrep)
		p.WriteByte('"')
	}
	if len(property.Language) > 0 {
		p.WriteString(`;LANGUAGE="`)
		p.WriteString(property.Language)
		p.WriteByte('"')
	}
	if len(property.Text) > 0 {
		p.WriteString(":\"")
		p.WriteString(property.Text)
		p.WriteByte('"')
	}
	p.WriteByte('\n')
	return p.String()
}
