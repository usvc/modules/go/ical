package properties

import "strings"

// ProductID implements 4.7.3 Product Identifier
//
// prodid     = "PRODID" pidparam ":" pidvalue CRLF
// pidparam   = *(";" xparam)
// pidvalue   = text
//
// See https://tools.ietf.org/html/rfc2445#section-4.7.3
type ProductID struct {
	Value []string
}

// IsValid implements the Property interface
func (p ProductID) IsValid() bool {
	return len(p.Value) > 0
}

// String returns the value as a valid iCalendar ProdID property
func (p ProductID) String() string {
	var s strings.Builder
	s.WriteString("PRODID:-//")
	for i := 0; i < len(p.Value)-1; i++ {
		value := p.Value[i]
		s.WriteString(value)
		s.WriteString("//")
	}
	s.WriteString(p.Value[len(p.Value)-1])
	s.WriteByte('\n')
	return s.String()
}
