package properties

import "strings"

// Contact implements 4.8.4.2 Contact
//
// contact    = "CONTACT" contparam ":" text CRLF
// contparam  = *((";" altrepparam) / (";" languageparam) / *(";" xparam))
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.4.2
type Contact struct {
	Altrep   string
	Language string
	Text     string
}

// IsValid implements the Property interface
func (property Contact) IsValid() bool {
	return len(property.Altrep) > 0 || len(property.Text) > 0
}

// String returns the value as a valid iCalendar Organizer property
func (property Contact) String() string {
	var p strings.Builder
	p.WriteString("CONTACT")
	if len(property.Altrep) > 0 {
		p.WriteString(`;ALTREP="`)
		p.WriteString(property.Altrep)
		p.WriteByte('"')
	}
	if len(property.Language) > 0 {
		p.WriteString(`;LANGUAGE="`)
		p.WriteString(property.Language)
		p.WriteByte('"')
	}
	if len(property.Text) > 0 {
		p.WriteByte(':')
		p.WriteString(property.Text)
	}
	p.WriteByte('\n')
	return p.String()
}
