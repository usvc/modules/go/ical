/*
Package properties implements the following properties from RFC2445:

4.7.3       Product Identifier
4.8.1.2     Categories
4.8.1.5     Description
4.8.1.7     Location
4.8.1.11    Status
4.8.1.12    Summary
4.8.2.2     Date/Time End
4.8.2.4     Date/Time Start
4.8.4.2     Contact
4.8.4.3     Organizer
4.8.4.6     Uniform Resource Locator
4.8.4.7     Unique Identifier
4.8.7.1     Date/Time Created
4.8.7.2     Date/Time Stamp
4.8.7.3     Last Modified
*/
package properties
