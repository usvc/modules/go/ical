package properties

import (
	"strings"
)

// Categories implements 4.8.1.2 Categories
//
// categories = "CATEGORIES" catparam ":" text *("," text) CRLF
// catparam   = *((";" languageparam ) / *(";" xparam))
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.1.2
type Categories []string

// IsValid implements the Property interface
func (property Categories) IsValid() bool {
	return len(property) > 0
}

// String returns the value as a valid iCalendar Categories property
func (property Categories) String() string {
	var p strings.Builder
	p.WriteString("CATEGORIES")
	p.WriteByte(':')
	for i := 0; i < len(property)-1; i++ {
		p.WriteString(property[i])
		p.WriteByte(',')
	}
	p.WriteString(property[len(property)-1])
	p.WriteByte('\n')
	return p.String()
}
