package properties

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type DescriptionTests struct {
	suite.Suite
}

func TestDescription(t *testing.T) {
	suite.Run(t, &DescriptionTests{})
}

func (t *DescriptionTests) TestMatchProperty() {
	var p Property = Description{}
	t.Equal("DESCRIPTION\n", p.String())
}

func (t *DescriptionTests) TestIsValid() {
	t.True(Description{Text: "t"}.IsValid())
	t.True(Description{Altrep: "a"}.IsValid())
	t.False(Description{}.IsValid())
	t.False(Description{Language: "l"}.IsValid())
}

func (t *DescriptionTests) TestStringWithParams() {
	expectedAltrep := "__test_altrep"
	expectedLanguage := "__test_language"
	expectedText := "__test_text"
	property := Description{
		Altrep: expectedAltrep,
	}
	t.Equal(fmt.Sprintf("DESCRIPTION;ALTREP=\"%s\"\n", expectedAltrep), property.String())
	property.Language = "__test_language"
	t.Equal(fmt.Sprintf("DESCRIPTION;ALTREP=\"%s\";LANGUAGE=\"%s\"\n", expectedAltrep, expectedLanguage), property.String())
	property.Text = expectedText
	t.Equal(fmt.Sprintf("DESCRIPTION;ALTREP=\"%s\";LANGUAGE=\"%s\":\"%s\"\n", expectedAltrep, expectedLanguage, expectedText), property.String())
}

func (t *DescriptionTests) TestStringWithoutParams() {
	expectedText := "__test_text"
	property := Description{
		Text: expectedText,
	}
	t.Equal(fmt.Sprintf("DESCRIPTION:\"%s\"\n", expectedText), property.String())
}
