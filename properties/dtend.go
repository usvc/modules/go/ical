package properties

import (
	"strings"
	"time"
)

// DTEnd implements 4.8.2.2 Date/Time End
//
// dtend      = "DTEND" dtendparam":" dtendval CRLF
// dtendparam = *((";" "VALUE" "=" ("DATE-TIME" / "DATE")) / (";" tzidparam) / *(";" xparam))
// dtendval   = date-time / date
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.2.2
type DTEnd struct {
	// Value specifies the starting date/date-time
	Value time.Time
	// TZID specifies the timezone ID
	TZID string
	// IsDate specifies whether the value should be a DATE or DATE-TIME value
	IsDate bool
}

// IsValid implements the Property interface
func (property DTEnd) IsValid() bool {
	return property.Value != time.Time{}
}

// String returns the value as a valid iCalendar DTEnd property
func (property DTEnd) String() string {
	var p strings.Builder
	p.WriteString("DTEND")

	if len(property.TZID) > 0 {
		p.WriteString(";TZID=")
		p.WriteString(property.TZID)
	}
	p.WriteByte(':')

	var format func(time.Time) string
	if property.IsDate {
		format = FormatDate
	} else if len(property.TZID) > 0 {
		format = FormatDateTimeLocal
	} else {
		format = FormatDateTime
	}
	p.WriteString(format(property.Value))
	p.WriteByte('\n')
	return p.String()
}
