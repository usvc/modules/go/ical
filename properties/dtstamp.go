package properties

import (
	"strings"
	"time"
)

// DTStamp implements 4.8.7.2 Date/Time Stamp
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.7.2
type DTStamp time.Time

// IsValid implements the Property interface
func (property DTStamp) IsValid() bool {
	return time.Time(property) != time.Time{}
}

// String returns the value as a valid iCalendar DTStamp property
func (property DTStamp) String() string {
	var p strings.Builder
	p.WriteString("DTSTAMP")
	p.WriteByte(':')
	p.WriteString(FormatDateTime(time.Time(property)))
	p.WriteByte('\n')
	return p.String()
}
