package properties

import (
	"strings"
	"time"
)

// Created implements 4.8.7.1 Date/Time Created
//
// created    = "CREATED" creaparam ":" date-time CRLF
// creaparam  = *(";" xparam)
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.7.1
type Created time.Time

// IsValid implements the Property interface
func (property Created) IsValid() bool {
	return time.Time(property) != time.Time{}
}

// String returns the value as a valid iCalendar Created property
func (property Created) String() string {
	var p strings.Builder
	p.WriteString("CREATED")
	p.WriteByte(':')
	p.WriteString(FormatDateTime(time.Time(property)))
	p.WriteByte('\n')
	return p.String()
}
