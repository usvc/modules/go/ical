package properties

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type CategoriesTests struct {
	suite.Suite
}

func TestCategories(t *testing.T) {
	suite.Run(t, &CategoriesTests{})
}

func (t *CategoriesTests) TestIsValid() {
	var categories Categories = []string{}
	t.False(categories.IsValid())
	categories = []string{"category 1"}
	t.True(categories.IsValid())
}

func (t *CategoriesTests) TestString() {
	var categories Categories = []string{"a b", "c"}
	t.Equal("CATEGORIES:a b,c\n", categories.String())
}
