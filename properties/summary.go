package properties

import "strings"

/*
Summary implements 4.8.1.12 Summary

summary    = "SUMMARY" summparam ":" text CRLF
summparam  = *((";" altrepparam) / (";" languageparam) / *(";" xparam))

See https://tools.ietf.org/html/rfc2445#section-4.8.1.12
*/
type Summary struct {
	Altrep   string
	Language string
	Text     string
}

// IsValid implements the Property interface
func (property Summary) IsValid() bool {
	return len(property.Altrep) > 0 || len(property.Text) > 0
}

// String returns the value as a valid iCalendar Summary property
func (property Summary) String() string {
	var p strings.Builder
	p.WriteString("SUMMARY")

	if len(property.Altrep) > 0 {
		p.WriteString(`;ALTREP="`)
		p.WriteString(property.Altrep)
		p.WriteByte('"')
	}
	if len(property.Language) > 0 {
		p.WriteString(`;LANGUAGE="`)
		p.WriteString(property.Language)
		p.WriteByte('"')
	}
	if len(property.Text) > 0 {
		p.WriteByte(':')
		p.WriteString(property.Text)
	}
	p.WriteByte('\n')
	return p.String()
}
