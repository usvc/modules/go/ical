package properties

import (
	"strings"
	"time"
)

// Property specifies a standardised interface which all structs
// defined here should adhere to
type Property interface {
	IsValid() bool
	String() string
}

// FormatDate formats the :input time according to section
// 4.3.4 Date of RFC2445
//
// See https://tools.ietf.org/html/rfc2445#section-4.3.4
func FormatDate(input time.Time) string {
	var formattedTime strings.Builder
	formattedTime.WriteString("VALUE=DATE:")
	formattedTime.WriteString(input.UTC().Format("20060102"))
	return formattedTime.String()
}

// FormatDateTime formats the :input time in Form #2 according
// to section 4.3.5 Date-Time of RFC2445
//
// See https://tools.ietf.org/html/rfc2445#section-4.3.5
func FormatDateTime(input time.Time) string {
	formattedTime := input.UTC().Format(time.RFC3339)
	formattedTime = strings.ReplaceAll(formattedTime, "-", "")
	formattedTime = strings.ReplaceAll(formattedTime, ":", "")
	return formattedTime
}

// FormatDateTimeLocal formats the :input time in Form #1
// according to section 4.3.5 Date-Time of RFC2445
//
// See https://tools.ietf.org/html/rfc2445#section-4.3.5
func FormatDateTimeLocal(input time.Time) string {
	formattedTime := input.Local().Format(time.RFC3339)
	formattedTime = strings.ReplaceAll(formattedTime, "-", "")
	formattedTime = strings.ReplaceAll(formattedTime, ":", "")
	formattedTime = strings.Split(formattedTime, "+")[0]
	return formattedTime
}
