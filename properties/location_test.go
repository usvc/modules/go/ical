package properties

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type LocationTests struct {
	suite.Suite
}

func TestLocation(t *testing.T) {
	suite.Run(t, &LocationTests{})
}

func (t *LocationTests) TestMatchProperty() {
	var p Property = Location{}
	t.Equal("LOCATION\n", p.String())
}

func (t *LocationTests) TestIsValid() {
	t.True(Location{Text: "t"}.IsValid())
	t.True(Location{Altrep: "a"}.IsValid())
	t.False(Location{}.IsValid())
	t.False(Location{Language: "l"}.IsValid())
}

func (t *LocationTests) TestStringWithParams() {
	expectedAltrep := "__test_altrep"
	expectedLanguage := "__test_language"
	expectedText := "__test_text"
	property := Location{
		Altrep: expectedAltrep,
	}
	t.Equal(fmt.Sprintf("LOCATION;ALTREP=\"%s\"\n", expectedAltrep), property.String())
	property.Language = "__test_language"
	t.Equal(fmt.Sprintf("LOCATION;ALTREP=\"%s\";LANGUAGE=\"%s\"\n", expectedAltrep, expectedLanguage), property.String())
	property.Text = expectedText
	t.Equal(fmt.Sprintf("LOCATION;ALTREP=\"%s\";LANGUAGE=\"%s\":\"%s\"\n", expectedAltrep, expectedLanguage, expectedText), property.String())
}

func (t *LocationTests) TestStringWithoutParams() {
	expectedText := "__test_text"
	property := Location{
		Text: expectedText,
	}
	t.Equal(fmt.Sprintf("LOCATION:\"%s\"\n", expectedText), property.String())
}
