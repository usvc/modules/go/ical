package properties

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type OrganizerTests struct {
	suite.Suite
}

func TestOrganizer(t *testing.T) {
	suite.Run(t, &OrganizerTests{})
}

func (t *OrganizerTests) TestMatchProperty() {
	var organizer Property
	organizer = Organizer{}
	t.Equal("ORGANIZER\n", organizer.String())
}

func (t *OrganizerTests) TestIsValid() {
	t.False(Organizer{}.IsValid())
	t.True(Organizer{Mailto: "email@address.com"}.IsValid())
}

func (t *OrganizerTests) TestStringWithoutParams() {
	expectedMailto := "email@address.com"
	organizer := Organizer{
		Mailto: expectedMailto,
	}
	t.Equal("ORGANIZER:MAILTO:\"email@address.com\"\n", organizer.String())
}

func (t *OrganizerTests) TestStringWithParams() {
	expectedCN := "cn"
	expectedDir := "dir"
	expectedSentBy := "sentby@address.com"
	expectedLanguage := "sg-EN"
	expectedMailto := "host@address.com"
	organizer := Organizer{
		CN:       expectedCN,
		Dir:      expectedDir,
		SentBy:   expectedSentBy,
		Language: expectedLanguage,
		Mailto:   expectedMailto,
	}
	t.Equal("ORGANIZER;CN=cn;DIR=\"dir\";SENT-BY=\"MAILTO:sentby@address.com\";LANGUAGE=sg-EN:MAILTO:\"host@address.com\"\n", organizer.String())
}
