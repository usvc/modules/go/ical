package properties

import (
	"github.com/google/uuid"
	"strings"
)

// UID implements 4.8.4.7 Unique Identifier
//
// uid        = "UID" uidparam ":" text CRLF
// uidparam   = *(";" xparam)
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.4.7
type UID string

// IsValid implements the Property interface
func (property UID) IsValid() bool {
	return len(property) > 0
}

// String returns the value as a valid iCalendar Organizer property
func (property UID) String() string {
	var p strings.Builder
	p.WriteString("UID")

	p.WriteByte(':')
	if len(property) > 0 {
		p.WriteString(string(property))
	} else {
		p.WriteString(uuid.New().String())
	}
	p.WriteByte('\n')
	return p.String()
}
