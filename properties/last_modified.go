package properties

import (
	"strings"
	"time"
)

// LastModified implements 4.8.7.3 Last Modified
//
// last-mod   = "LAST-MODIFIED" lstparam ":" date-time CRLF
// lstparam   = *(";" xparam)
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.7.3
type LastModified time.Time

// IsValid implements the Property interface
func (property LastModified) IsValid() bool {
	return time.Time(property) != time.Time{}
}

// String returns the value as a valid iCalendar Last Modified property
func (property LastModified) String() string {
	var p strings.Builder
	p.WriteString("LAST-MODIFIED")
	p.WriteByte(':')
	p.WriteString(FormatDateTime(time.Time(property)))
	p.WriteByte('\n')
	return p.String()
}
