package properties

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type DTStampTests struct {
	suite.Suite
}

func TestDTStamp(t *testing.T) {
	suite.Run(t, &DTStampTests{})
}
func (t *DTStampTests) TestMatchProperty() {
	var dtstamp Property
	dtstamp = DTStamp{}
	t.Equal("DTSTAMP:00010101T000000Z\n", dtstamp.String())
}

func (t *DTStampTests) TestIsValid() {
	t.False(DTStamp{}.IsValid())
	location, _ := time.LoadLocation("UTC")
	t.True(DTStamp(time.Date(1, 1, 1, 1, 1, 1, 1, location)).IsValid())
}

func (t *DTStampTests) TestString() {
	utcLocation, _ := time.LoadLocation("UTC")
	testTime := time.Date(2019, 12, 13, 14, 59, 1, 2, utcLocation)
	dtStamp := DTStamp(testTime)
	t.Equal("DTSTAMP:20191213T145901Z\n", dtStamp.String())
}
