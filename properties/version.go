package properties

import "strings"

// Version implements 4.7.4 Version
//
// version    = "VERSION" verparam ":" vervalue CRLF
// verparam   = *(";" xparam)
// vervalue   = "2.0" / maxver / (minver ";" maxver)
// minver     = <A IANA registered iCalendar version identifier>
// maxver     = <A IANA registered iCalendar version identifier>
//
// See https://tools.ietf.org/html/rfc2445#section-4.7.4
type Version string

// IsValid implements the Property interface
func (p Version) IsValid() bool {
	return len(p) > 0
}

// String returns the value as a valid iCalendar Version property
func (p Version) String() string {
	var s strings.Builder
	s.WriteString("VERSION")
	s.WriteByte(':')
	version := "2.0"
	if len(p) > 0 {
		version = string(p)
	}
	s.WriteString(version)
	s.WriteByte('\n')
	return s.String()
}
