package properties

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type ContactTests struct {
	suite.Suite
}

func TestContact(t *testing.T) {
	suite.Run(t, &ContactTests{})
}

func (t *ContactTests) TestMatchProperty() {
	var property Property = Contact{}
	t.Equal("CONTACT\n", property.String())
}

func (t *ContactTests) TestIsValid() {
	t.True(Contact{Text: "t"}.IsValid())
	t.True(Contact{Altrep: "a"}.IsValid())
	t.False(Contact{}.IsValid())
	t.False(Contact{Language: "l"}.IsValid())
}

func (t *ContactTests) TestStringWithParams() {
	expectedAltrep := "__test_altrep"
	expectedLanguage := "__test_language"
	expectedText := "__test_text"
	property := Contact{
		Altrep: expectedAltrep,
	}
	t.Equal(fmt.Sprintf("CONTACT;ALTREP=\"%s\"\n", expectedAltrep), property.String())
	property.Language = "__test_language"
	t.Equal(fmt.Sprintf("CONTACT;ALTREP=\"%s\";LANGUAGE=\"%s\"\n", expectedAltrep, expectedLanguage), property.String())
	property.Text = expectedText
	t.Equal(fmt.Sprintf("CONTACT;ALTREP=\"%s\";LANGUAGE=\"%s\":%s\n", expectedAltrep, expectedLanguage, expectedText), property.String())
}

func (t *ContactTests) TestStringWithoutParams() {
	expectedText := "__test_text"
	property := Contact{
		Text: expectedText,
	}
	t.Equal(fmt.Sprintf("CONTACT:%s\n", expectedText), property.String())
}
