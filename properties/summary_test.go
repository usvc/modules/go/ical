package properties

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type SummaryTests struct {
	suite.Suite
}

func TestSummary(t *testing.T) {
	suite.Run(t, &SummaryTests{})
}

func (t *SummaryTests) TestMatchProperty() {
	var p Property = Summary{}
	t.Equal("SUMMARY\n", p.String())
}

func (t *SummaryTests) TestIsValid() {
	t.True(Summary{Text: "t"}.IsValid())
	t.True(Summary{Altrep: "a"}.IsValid())
	t.False(Summary{}.IsValid())
	t.False(Summary{Language: "l"}.IsValid())
}

func (t *SummaryTests) TestStringWithParams() {
	expectedAltrep := "__test_altrep"
	expectedLanguage := "__test_language"
	expectedText := "__test_text"
	property := Summary{
		Altrep: expectedAltrep,
	}
	t.Equal(fmt.Sprintf("SUMMARY;ALTREP=\"%s\"\n", expectedAltrep), property.String())
	property.Language = "__test_language"
	t.Equal(fmt.Sprintf("SUMMARY;ALTREP=\"%s\";LANGUAGE=\"%s\"\n", expectedAltrep, expectedLanguage), property.String())
	property.Text = expectedText
	t.Equal(fmt.Sprintf("SUMMARY;ALTREP=\"%s\";LANGUAGE=\"%s\":%s\n", expectedAltrep, expectedLanguage, expectedText), property.String())
}

func (t *SummaryTests) TestStringWithoutParams() {
	expectedText := "__test_text"
	property := Summary{
		Text: expectedText,
	}
	t.Equal(fmt.Sprintf("SUMMARY:%s\n", expectedText), property.String())
}
