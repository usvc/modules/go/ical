package properties

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
)

type UIDTests struct {
	suite.Suite
}

func TestUID(t *testing.T) {
	suite.Run(t, &UIDTests{})
}

func (t *UIDTests) TestMatchProperty() {
	var p Property = UID("__test")
	t.Equal("UID:__test\n", p.String())
}

func (t *UIDTests) TestIsValid() {
	t.False(UID("").IsValid())
	t.True(UID("a").IsValid())
}

func (t *UIDTests) TestStringWithoutUID() {
	var uid UID
	t.True(len(uid.String()) > 4)
}

func (t *UIDTests) TestStringWithUID() {
	uid := UID(uuid.New().String())
	t.True(len(uid.String()) > 4)
}
