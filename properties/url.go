package properties

import (
	"strings"
)

// URL implements 4.8.4.6 Uniform Resource Locator
//
// url        = "URL" urlparam ":" uri CRLF
// urlparam   = *(";" xparam)
//
// See https://tools.ietf.org/html/rfc2445#section-4.8.4.6
type URL string

// IsValid implements the Property interface
func (property URL) IsValid() bool {
	return len(property) > 0
}

// String returns the value as a valid iCalendar URL property
func (property URL) String() string {
	var p strings.Builder
	p.WriteString("URL")
	p.WriteByte(':')
	p.WriteString(string(property))
	p.WriteByte('\n')
	return p.String()
}
