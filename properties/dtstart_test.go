package properties

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type DTStartTests struct {
	suite.Suite
}

func TestDTStart(t *testing.T) {
	suite.Run(t, &DTStartTests{})
}
func (t *DTStartTests) TestMatchProperty() {
	var dtStart Property
	dtStart = DTStart{}
	t.Equal("DTSTART:00010101T000000Z\n", dtStart.String())
}

func (t *DTStartTests) TestIsValid() {
	t.False(DTStart{}.IsValid())
	t.False(DTStart{Value: time.Time{}}.IsValid())
	utcLocation, _ := time.LoadLocation("UTC")
	t.True(DTStart{Value: time.Date(1, 1, 1, 1, 1, 1, 1, utcLocation)}.IsValid())
}

func (t *DTStartTests) TestString() {
	utcLocation, _ := time.LoadLocation("UTC")
	testTime := time.Date(2019, 12, 13, 14, 59, 1, 2, utcLocation)
	dtStart := DTStart{
		Value: testTime,
	}
	t.Equal("DTSTART:20191213T145901Z\n", dtStart.String())
}

func (t *DTStartTests) TestStringWithParams() {
	singapore, _ := time.LoadLocation("Asia/Singapore")
	testTime := time.Date(2019, 12, 13, 14, 59, 1, 2, singapore)
	dtStart := DTStart{
		Value: testTime,
		TZID:  "Asia/Singapore",
	}
	t.Equal("DTSTART;TZID=Asia/Singapore:20191213T145901\n", dtStart.String())
}
