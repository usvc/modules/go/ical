package properties

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type LastModifiedTests struct {
	suite.Suite
}

func TestLastModified(t *testing.T) {
	suite.Run(t, &LastModifiedTests{})
}
func (t *LastModifiedTests) TestMatchProperty() {
	var lastModified Property
	lastModified = LastModified{}
	t.Equal("LAST-MODIFIED:00010101T000000Z\n", lastModified.String())
}

func (t *LastModifiedTests) TestIsValid() {
	t.False(LastModified{}.IsValid())
	location, _ := time.LoadLocation("UTC")
	t.True(LastModified(time.Date(1, 1, 1, 1, 1, 1, 1, location)).IsValid())
}

func (t *LastModifiedTests) TestString() {
	utcLocation, _ := time.LoadLocation("UTC")
	testTime := time.Date(2019, 12, 13, 14, 59, 1, 2, utcLocation)
	lastModified := LastModified(testTime)
	t.Equal("LAST-MODIFIED:20191213T145901Z\n", lastModified.String())
}
