get_deps:
	go mod vendor -v

test:
	@$(MAKE) test_client
	@$(MAKE) test_event
	@$(MAKE) test_properties

test_client:
	go test -v ./client

test_event:
	go test -v ./event

test_properties:
	go test -v ./properties
