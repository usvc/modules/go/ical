package server

import (
	"net/http"
	"strings"
	"time"
	"gitlab.com/usvc/go/modules/calendar/constants"
)

var (
	MethodsAllowedForDiscoverHTTP []string = []string{
		constants.MethodOptions,
		constants.MethodGet,
		constants.MethodHead,
		constants.MethodPost,
		constants.MethodPut,
		constants.MethodDelete,
	}
	MethodsAllowedForDiscoverDAV []string = []string{
		constants.MethodTrace,
		constants.MethodCopy,
		constants.MethodMove,
		constants.MethodPropfind,
		constants.MethodProppatch,
		constants.MethodLock,
		constants.MethodUnlock,
		constants.MethodReport,
		constants.MethodACL,
	}
)

// AddDiscoverResponseSuccessHeaders implements RFC4791 Section 5.1.1
// on discovery of calendar access support.
//
// See https://tools.ietf.org/html/rfc4791#section-5.1.1
func AddDiscoverResponseSuccessHeaders(res http.ResponseWriter) {
	res.WriteHeader(http.StatusOK)
	res.Header().Add("Allow", strings.Join(MethodsAllowedForDiscoverHTTP, ","))
	res.Header().Add("Allow", strings.Join(MethodsAllowedForDiscoverDAV, ","))
	res.Header().Add("DAV", "1, 2, access-control, calendar-access")
	res.Header().Add("Date", time.Now().UTC().Format("Mon, 2 Jan 2006, 15:04:05 GMT"))
	res.Header().Add("Content-Length", "0")
}
