package constants

const (
	MethodOptions   = "OPTIONS"
	MethodGet       = "GET"
	MethodHead      = "HEAD"
	MethodPost      = "POST"
	MethodPut       = "PUT"
	MethodDelete    = "DELETE"
	MethodTrace     = "TRACE"
	MethodCopy      = "COPY"
	MethodMove      = "MOVE"
	MethodPropfind  = "PROPFIND"
	MethodProppatch = "PROPPATCH"
	MethodLock      = "LOCK"
	MethodUnlock    = "UNLOCK"
	MethodReport    = "REPORT"
	MethodACL       = "ACL"
)
