package constants

const (
	StatusCancelled       = "CANCELLED"
	StatusEventTentative  = "TENTATIVE"
	StatusEventConfirmed  = "CONFIRMED"
	StatusTodoNeedsAction = "NEEDS-ACTION"
	StatusTodoCompleted   = "COMPLETED"
	StatusTodoInProcess   = "IN-PROCESS"
	StatusJournalDraft    = "DRAFT"
	StatusJournalFinal    = "FINAL"
)
