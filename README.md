# ICal Module

This is a Go module for handling the iCalendar format as [documented in RFC2445](https://tools.ietf.org/html/rfc2445)

# Usage

## Creating a new calendar

```go
import "gitlab.com/usvc/modules/go/ical/calendar"
cal := calendar.New()
```

## Creating a new event

```go
import "gitlab.com/usvc/modules/go/ical/event"
ev := event
  .New()
  .WithTitle("title of event")
```

## Adding events to calendar

```go
import "gitlab.com/usvc/modules/go/ical/calendar"
import "gitlab.com/usvc/modules/go/ical/event"

cal := calendar.New()
cal.AddEvent(
  event.New()
    .WithTitle("christmas 2019")
    .WithDescription("a day of celebration")
    .FromDatetime(2019, 12, 25, 0, 0, 0)
    .ToDateTime(2019, 12, 25, 23, 59, 59)
)
```

## Exporting as an ICal file

```go
import "gitlab.com/usvc/modules/go/ical/calendar"

cal := calendar.New()
// ...
data := cal.String()
```

# Contributing

## Retrieving dependencies

We use `go mod` for dependency management. To pull in required dependencies, run:

```sh
go mod vendor;
# OR
make get_deps;
```

## Running tests

We use `go test` for testing. To run automated tests, run:

```sh
go test ./...;
# OR
make test;
```

# License

Code is licensed under the [MIT license](./LICENSE)
