package calendar

import (
	"strings"

	"gitlab.com/usvc/modules/go/calendar/event"
	"gitlab.com/usvc/modules/go/calendar/properties"
)

// Calendar implements 4.4 iCalendar Object
//
// See https://tools.ietf.org/html/rfc2445#section-4.4
type Calendar struct {
	Events    []event.Event
	ProductID *properties.ProductID
	Version   *properties.Version
}

// IsValid returns true if the calendar object is valid
func (calendar *Calendar) IsValid() bool {
	return calendar.ProductID != nil && calendar.ProductID.IsValid()
}

// String returns the Calendar instance as an iCalendar
func (calendar *Calendar) String() string {
	var p strings.Builder
	p.WriteString("BEGIN:VCALENDAR\n")
	if calendar.ProductID != nil && calendar.ProductID.IsValid() {
		p.WriteString(calendar.ProductID.String())
	}
	if calendar.Version != nil && calendar.Version.IsValid() {
		p.WriteString(calendar.Version.String())
	}
	for i := 0; i < len(calendar.Events); i++ {
		p.WriteString(calendar.Events[i].String())
	}
	p.WriteString("END:VCALENDAR\n")
	return p.String()
}
